#include "functions.cpp"

void get_time(char* buf)
{
	char	   str[80];	
	time_t     now = time(0);
	struct tm  tstruct;
	tstruct = *localtime(&now);
	strftime(str, sizeof(str), "%Y-%m-%d %X", &tstruct);
	strcpy(buf, str);
}

bool server_actions(int &clientSocket, session &current_session)
{
	char msg[BUFFER_SIZE];
	memset(&msg, 0, sizeof(msg));
	int bytesReceived, bytesSend;

    bytesReceived = recv(clientSocket, (char*)&msg, sizeof(msg), 0);
    if (!checkConnection(bytesReceived)) return false;

    string cmd = msg;
    cmd = cmd.substr(cmd.find_first_of(" ")+1);
    cout << cmd << endl;

    if (!strcmp(strtok(msg," "), "exit")) {
    	cout << "Client has quit the session" << endl;
    	return false;
    } else if (!strcmp(strtok(msg," "), "echo")) {
    	strcpy(msg, cmd.c_str());
    	bytesSend = send(clientSocket, (char*)&msg, strlen(msg), 0);
    	if (!checkConnection(bytesSend)) return false;
    } else if (!strcmp(strtok(msg," "), "time")) {
    	char buf[80];
 		get_time(buf);
	    bytesSend = send(clientSocket, (char*)&buf, sizeof(buf), 0);
	    if (!checkConnection(bytesSend)) return false;
    } else if (!strcmp(strtok(msg," "), "download")) {
    	current_session.direction = 'i';
        current_session.filename = cmd;
    	if (!sendFile(clientSocket, current_session)) return false;
    } else if (!strcmp(strtok(msg," "), "upload")) {
    	current_session.direction = 'o';
        current_session.filename = cmd;
    	if (!recvFile(clientSocket, current_session)) return false;
    } else {
    	bytesSend = send(clientSocket, "unknown command", 15, 0);
	    if (!checkConnection(bytesSend)) return false;
    }

    return true;
}

bool new_connection(int &ourSocket, session &current_session)
{
	int clientSocket = accept(ourSocket, NULL, NULL);

 	int bytesReceived, bytesSend;
 	char msg[BUFFER_SIZE];
 	memset(&msg, 0, sizeof(msg));

 	bytesReceived = recv(clientSocket, (char*)&msg, sizeof(msg), 0);
 	if (!checkConnection(bytesReceived)) return true;

   	if ((current_session.username == msg) && (current_session.load == true)) {
    	char num[2];
    	memset(&num, 0, sizeof(num));
    	num[0] = current_session.last_packet;
    	num[1] = current_session.direction;
    	bytesSend = send(clientSocket, (char*)&num, sizeof(num), 0);
    	if (current_session.direction == 'i') {
    		if (!sendFile(clientSocket, current_session)) return true;
    	} else {
    		if (!recvFile(clientSocket, current_session)) return true;
    	}
        bytesSend = send(clientSocket, (char*)&current_session.filename, sizeof(current_session.filename), 0);
    } else {
    	char num[2];
    	memset(&num, 0, sizeof(num));
 		current_session.username = msg;
 		current_session.last_packet = 0;
 		num[0] = current_session.last_packet;
 		bytesSend = send(clientSocket, (char*)&num, sizeof(num), 0);
 	}
	
 	while(server_actions(clientSocket, current_session));

 	close(clientSocket);
 	return true;
}

int main(int argc, char *argv[])
{
	int ourSocket;
	struct sockaddr_in ourSocketAddress;
	ourSocketAddress.sin_family = AF_INET;
	ourSocketAddress.sin_port = htons(PORT);
	ourSocketAddress.sin_addr.s_addr = INADDR_ANY;
	ourSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	bind(ourSocket, (struct sockaddr*)&ourSocketAddress, sizeof(ourSocketAddress));
	listen(ourSocket, 1);

	session current_session;

	while(new_connection(ourSocket, current_session));

 	close(ourSocket);
 	return 0; 
}
