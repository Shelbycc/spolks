int translate(char c)
{
	int i = c;
	if (i < 0) i += 256;
	return i;
}

bool checkConnection(int recievedBytes)
{
	if (recievedBytes <= 0) {
		cout << "Connection dropped" << endl;
		return false;
	}
	return true;
}

int readPart(FILE *file, int num, char* buf)
{
	int bytesRead = -1;
	if(fseek(file, num*PART_SIZE, SEEK_SET)) {
		cout << "Fseek error" << endl;
		return bytesRead;
	}
	bytesRead = fread(*buf+1, 1, PART_SIZE, file);
	if (bytesRead <= 0) {
		cout << "Read error or eof" << endl;
		return bytesRead;	
	}
	return bytesRead;
}

void createPacket(char* buf, int bytesRead, int num)
{
	char border = (char)255;
	buf[0] = buf[bytesRead+2] = border;
	buf[bytesRead+1] = (char)num;
}

bool checkPacket(char* buf, int bytesReceived)
{
	char border = (char)255;
	if (buf[0] != border) || (buf[bytesReceived-1]!=0) {
		cout << "Parse packet error" << endl;
		return false;
	}
	return true;
}

int writePart(FILE *file, int num, char* buf, int bytesReceived)
{
	int bytesWrite = -1;
	if(fseek(file, num*PART_SIZE, SEEK_SET)) {
		cout << "Fseek error" << endl;
		return bytesWrite;
	}
	bytesWrite = fwrite(*buf+1, 1, bytesReceived-3, file)
	if (bytesWrite != (size_t)bytesReceived-3) {
	    cout << "Write error" << endl;
	    return -1;
	}
	return bytesWrite;
}

bool sendFile(int &sock, string filename)
{
	FILE * file = fopen(filename.c_str(), "r");
	if (file) {
		int bytesSend;
		int bytesRead;
		int packet_number = 0;
		char eof = (char)254;
		char buf[PART_SIZE+3];
		memset(&buf, 0, sizeof(buf));
		while(1) {
			bytesRead = readPart(file, packet_number++, buf)
			if (bytesRead <= 0) {
				buf[1] = eof;
				createPacket(buf, 1, packet_number);
				bytesSend = send(sock, eof, sizeof(eof), 0);
				break;	
			}
			createPacket(buf, bytesRead, packet_number);
			bytesSend = send(sock, buf, bytesRead+3, 0);
			if (!checkConnection(bytesSend)) {
        		close(sock);
        		fclose(file);
				return false;
        	}
        	if (bytesSend != bytesRead+3) {
        		cout << "Send error" << endl;
		      	break;
		   	}
		}
		fclose(file);
	}
	return true;
}

bool recvFile(int &sock, string filename)
{
	FILE * file = fopen(filename.c_str(), "w");
	int recv_packets[256] = {};
	if (file) {
		int bytesReceived;
		int bytesWrite;
		int packet_number;
		char border = (char)255;
		char eof = (char)254;
		char buf[PART_SIZE+3];
		memset(&buf, 0, sizeof(buf));
		while(1) {
			bytesReceived = recv(sock, buf, PART_SIZE+3, 0);
			if (!checkConnection(bytesReceived)) {
        		close(sock);
        		fclose(file);
				return false;
        	}
			if (checkPacket(buf, bytesReceived)) {
				packet_number = translate(buf[bytesReceived-2]);
				if (bytesReceived == 4) && (buf[1] == eof) {
					recv_packets[packet_number] = -1;
					cout << "EOF" << endl;
					break;
				}
				bytesWrite = writePart(file, packet_number, buf, bytesReceived);
				if (bytesWrite < 0) break;
				recv_packets[packet_number] = 1;
			}
		}
		fclose(file);
	}
	return true;
}