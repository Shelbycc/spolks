#include "functions.cpp"

bool login(int &ourSocket, session &current_session)
{
	char msg[BUFFER_SIZE];
	memset(&msg, 0, sizeof(msg));
	int bytesSend;
	string data;

	cout << "Enter username:" << endl;
	getline(cin, data);
    strcpy(msg, data.c_str());
    current_session.username = msg;
    bytesSend = send(ourSocket, (char*)&msg, strlen(msg), 0);
    if (!checkConnection(bytesSend)) return false;
    return true;
}

void check_load(int &ourSocket, session &current_session)
{
	char num[2];
	char msg[BUFFER_SIZE];
	memset(&num, 0, sizeof(num));
	memset(&num, 0, sizeof(msg));
	int bytesReceived;

	bytesReceived = recv(ourSocket, (char*)&num, sizeof(num), 0);
	current_session.last_packet = (int)num[0];
	current_session.direction = (int)num[1];
	if (current_session.last_packet != 0){
		bytesReceived = recv(ourSocket, (char*)&msg, sizeof(msg), 0);
		current_session.filename = msg;
		if (current_session.direction == 'i'){
			recvFile(ourSocket, current_session);
		}else{
			sendFile(ourSocket, current_session);
		}
	}
}

bool client_actions(int &ourSocket, session &current_session)
{
	char msg[BUFFER_SIZE];
	memset(&msg, 0, sizeof(msg));
	int bytesReceived, bytesSend;
	string cmd;

	getline(cin, cmd);
    strcpy(msg, cmd.c_str());

    string filename = cmd.substr(cmd.find_first_of(" ")+1);

    bytesSend = send(ourSocket, (char*)&msg, strlen(msg), 0);
    if (!checkConnection(bytesSend)) return false;
    if(!strcmp(strtok(msg," "), "exit")) return false;
    if(!strcmp(strtok(msg," "), "download")) {
    	current_session.filename = filename;
    	if (!recvFile(ourSocket, current_session)) return false;
    } else if(!strcmp(strtok(msg," "), "upload")) {
    	current_session.filename = filename;
    	if (!sendFile(ourSocket, current_session)) return false;
    } else {
    	memset(&msg, 0, sizeof(msg));//clear the buffer
    	bytesReceived = recv(ourSocket, (char*)&msg, sizeof(msg), 0);
    	if (!checkConnection(bytesReceived)) return false;
    	cout << msg << endl;
	}
	return true;
}

int main(int argc, char *argv[])
{
	int ourSocket;
	struct sockaddr_in ourSocketAddress;
	ourSocketAddress.sin_family = AF_INET;
	ourSocketAddress.sin_port = htons(PORT);
	ourSocketAddress.sin_addr.s_addr = INADDR_ANY;
	ourSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	ourSocketAddress.sin_addr.s_addr = inet_addr("127.0.0.1");
	int error = connect(ourSocket, (struct sockaddr*)&ourSocketAddress, sizeof(ourSocketAddress));

	session current_session;

	while(!login(ourSocket, current_session));
    check_load(ourSocket, current_session);
	while(client_actions(ourSocket, current_session));

	close(ourSocket);
	return 0;
}
