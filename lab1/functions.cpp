#include <iostream>
#include <string>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netdb.h>
#include <sys/uio.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <fcntl.h>

#include <ctime>
#define PART_SIZE 512
#define PORT 8081 

using namespace std;

int translate(char c)
{
	int i = c;
	if (i < 0) i += 256;
	return i;
}

bool checkConnection(int recievedBytes)
{
	if (recievedBytes <= 0) {
		cout << "Connection dropped" << endl;
		return false;
	}
	return true;
}

int readPart(FILE *file, int num, char* buf)
{
	int bytesRead = -1;
	if(fseek(file, num*PART_SIZE, SEEK_SET)) {
		cout << "Fseek error" << endl;
		return bytesRead;
	}
	bytesRead = fread(buf+1, 1, PART_SIZE, file);
	if (bytesRead <= 0) {
		cout << "Read error or eof" << endl;
		return bytesRead;	
	}
	return bytesRead;
}

void createPacket(char* buf, int bytesRead, int num)
{
	char border = (char)255;
	buf[0] = buf[bytesRead+2] = border;
	buf[bytesRead+1] = (char)num;
}

bool checkPacket(char* buf, int bytesReceived)
{
	char border = (char)255;
	if ((buf[0] != border) || (buf[bytesReceived-1] != border)) {
		cout << "Parse packet error" << endl;
		return false;
	}
	return true;
}

int writePart(FILE *file, int num, char* buf, int bytesReceived)
{
	int bytesWrite = -1;
	if(fseek(file, num*PART_SIZE, SEEK_SET)) {
		cout << "Fseek error" << endl;
		return bytesWrite;
	}
	bytesWrite = fwrite(buf+1, 1, bytesReceived-3, file);
	if (bytesWrite != (size_t)bytesReceived-3) {
	    cout << "Write error" << endl;
	    return -1;
	}
	return bytesWrite;
}

bool sendFile(int &sock, string filename)
{
	FILE * file = fopen(filename.c_str(), "r");
	int bytesSend;
	int bytesRead;
	int packet_number = 0;
	char eof = (int)254;
	char buf[PART_SIZE+3];
	memset(&buf, 0, sizeof(buf));

	if (file) {
		while(1) {
			bytesRead = readPart(file, packet_number, buf);
			if (bytesRead <= 0) {
				buf[1] = eof;
				createPacket(buf, 1, packet_number++);
				bytesSend = send(sock, buf, 4, 0);
				break;	
			}
			createPacket(buf, bytesRead, packet_number++);
			bytesSend = send(sock, buf, bytesRead+3, 0);
			cout << bytesSend << endl;
			cout << packet_number-1 << endl;
			if (!checkConnection(bytesSend)) {
        		fclose(file);
				return false;
        	}
        	if (bytesSend != bytesRead+3) {
        		cout << "Send error" << endl;
		      	break;
		   	}
		}
		fclose(file);
	}

	recv(sock, buf, PART_SIZE+3, 0);
	if (!strcmp(strtok(buf," "), "ok")) {
		return true;
	} else {
		packet_number = translate(buf[0]);
		bytesRead = readPart(file, packet_number, buf);
		createPacket(buf, bytesRead, packet_number);
		bytesSend = send(sock, buf, bytesRead+3, 0);
		cout << bytesSend << endl;
	}

	return true;
}

bool recvFile(int &sock, string filename)
{
	FILE * file = fopen(filename.c_str(), "w");
	int recv_packets[256] = {};
	int bytesReceived;
	int bytesWrite;
	int packet_number;
	char border = 255;
	char eof = 254;
	char buf[PART_SIZE+3];
	memset(&buf, 0, sizeof(buf));

	if (file) {
		while(1) {
			bytesReceived = recv(sock, buf, PART_SIZE+3, 0);
			cout << bytesReceived << endl;
			if (!checkConnection(bytesReceived)) {
        		fclose(file);
				return false;
        	}
			if (checkPacket(buf, bytesReceived)) {
				packet_number = translate(buf[bytesReceived-2]);
				if ((bytesReceived == 4) && (buf[1] == eof)) {
					recv_packets[packet_number] = -1;
					cout << "EOF" << endl;
					break;
				}
				cout << packet_number << endl;
				bytesWrite = writePart(file, packet_number, buf, bytesReceived);
				if (bytesWrite < 0) break;
				recv_packets[packet_number] = 1;
			}
		}
		fclose(file);
	}

	bool flag = true;
	int i = 0;
	while (recv_packets[i] >= 0) { 
		if (recv_packets[i] == 0) {
			char c = i;
			send(sock, (char*)&c, 1, 0);
			bytesReceived = recv(sock, buf, PART_SIZE+3, 0);
			cout << bytesReceived << endl;
			if (checkPacket(buf, bytesReceived)) {
				packet_number = translate(buf[bytesReceived-2]);
				cout << packet_number << endl;
				bytesWrite = writePart(file, packet_number, buf, bytesReceived);
				recv_packets[packet_number] = 1;
			}
		}
	}
	if (flag) send(sock, "ok", 2, 0);
	return true;
}